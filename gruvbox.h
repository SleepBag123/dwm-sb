static const char norm_fg[] = "#fbf1c7";
static const char norm_bg[] = "#3c3836";
static const char norm_border[] = "#fbf1c7";

static const char sel_fg[] = "#fbf1c7";
static const char sel_bg[] = "#458588";
static const char sel_border[] = "#cc241d";

static const char urg_fg[] = "#d2d7d9";
static const char urg_bg[] = "#999E72";
static const char urg_border[] = "#999E72";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
};
